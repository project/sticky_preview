STICKY PREVIEW

This module provides sticky preview box on node add/edit page. You can enable this feature for each content type on
content type setting page. Also you can include to preview only those fields that you want.

Supported input types:
1. textarea
2. text
3. radio
4. checbox
5. select
