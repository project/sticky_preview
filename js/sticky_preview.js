(function ($) {
  /**
   * Class containing functionality for Sticky Preview.
   */
  Drupal.stickyPreview = {};

  /**
   * Process input object.
   */
  Drupal.stickyPreview.stickyPreviewProcessInput = function(name) {
    var selector = '',
        text = '';

    if (name == 'title') {
      // For title 'field' we should use strict match by 'name' attr.
      selector = '[name="' + name + '"]';
    } else {
      // Selector by 'name' attribute like '[name*="some_field_name["]'.
      // We need last bracket in selector to prevent false positive matches.
      // Fore example: 'some_field_name' and 'some_field_name_sub_str'.
      selector = '[name*="' + name + '["]';
    }

    var $fieldInput = $(selector).filter(':not([type=hidden])');

    // Get result text from each of input types.
    switch ($fieldInput.attr('type')) {
      case 'textarea':
      case 'text':
        text = Drupal.stickyPreview.stickyPreviewProcessTextWidgets($fieldInput);
        break;

      case 'select-one':
      case 'select-multiple':
        text = Drupal.stickyPreview.stickyPreviewProcessSelectWidgets($fieldInput);
        break;

      case 'radio':
        text = Drupal.stickyPreview.stickyPreviewProcessRadioWidgets($fieldInput);
        break;

      case 'checkbox':
        text = Drupal.stickyPreview.stickyPreviewProcessCheckboxWidgets($fieldInput);
        break;
    }

    return text;
  };

  /**
   * Get value from text widgets.
   */
  Drupal.stickyPreview.stickyPreviewProcessTextWidgets = function($fieldInput) {
    var values = [];

    // Rid of selectors with 'weights' for multiple-value text widgets.
    $fieldInput.filter('textarea, input[type="text"]').each(function() {
      values.push($(this).val());
    });

    return Drupal.stickyPreview.stickyPreviewGetTextResult(values);
  };

  /**
   * Get value from select widgets.
   */
  Drupal.stickyPreview.stickyPreviewProcessSelectWidgets = function($fieldInput) {
    var values = [];

    $fieldInput.each(function() {
      $(this).find('option:selected').each(function(i, selected) {
        var $selectedOption = $(selected),
          value = $selectedOption.val(),
          text = $selectedOption.text();

        // Push empty value if '- Select -' or '- None -' option is selected.
        if (!value.length || value == '_none') {
          text = '';
        }

        values.push(text);
      });
    });

    return Drupal.stickyPreview.stickyPreviewGetTextResult(values);
  };

  /**
   * Get value from radio widgets.
   */
  Drupal.stickyPreview.stickyPreviewProcessRadioWidgets = function($fieldInput) {
    var values = [];

    values.push($fieldInput.parent().find('input:radio:checked').next('label:first').text());

    return Drupal.stickyPreview.stickyPreviewGetTextResult(values);
  };

  /**
   * Get value from checkbox widgets.
   */
  Drupal.stickyPreview.stickyPreviewProcessCheckboxWidgets = function($fieldInput) {
    var values = [];

    $fieldInput.each(function() {
      var value = $(this).parent().find('input:checkbox:checked').next('label:first').text();

      if (value) {
        values.push(value);
      }
    });

    if (!values.length) {
      values.push('');
    }

    return Drupal.stickyPreview.stickyPreviewGetTextResult(values);
  };

  /**
   * Returns result text from processed inputs.
   */
  Drupal.stickyPreview.stickyPreviewGetTextResult = function(values) {
    var result = '';

    // Escape html special chars.
    for (var i = 0; i < values.length; i++) {
      values[i] = $('<div/>').text(values[i]).html();
    }

    // Rid of empty values in array.
    if (values.length > 1) {
      for (var i = 0; i < values.length; i++) {
        if (values[i] == '') {
          values.splice(i, 1);
        }
      }
    }

    if (values.length > 1) {
      result = Drupal.stickyPreview.stickyPreviewGenerateList(values);
    } else {
      result = values.pop();
    }

    return result;
  };

  /**
   * Returns html list from array.
   */
  Drupal.stickyPreview.stickyPreviewGenerateList = function(items) {
    var list = '<ul class="items-list">';

    for (var i = 0; i < items.length; i++) {
      list += '<li class="item">' + items[i] + '</li>';
    }

    return list + '</ul>';
  };

  /**
   * Returns different events for different input types.
   */
  Drupal.stickyPreview.getEventForInput = function($fieldInput) {
    var type = $fieldInput.attr('type'),
        isAriaAutocomplete = $fieldInput.attr('aria-autocomplete'),
        event = 'input';

    switch (type) {
      case 'radio':
      case 'checkbox':
        event = 'click';
        break;

      case 'select-one':
      case 'select-multiple':
        event = 'change click';
        break;
    }

    // Paste value from auto-complete text widgets only when focus lost.
    if (isAriaAutocomplete) {
      event = 'focusout';
    }

    return event;
  };

  /**
   * Behaviour provides preview functionality.
   */
  Drupal.behaviors.stickyPreview = {
    attach: function (context, settings) {
      // Add preview box to DOM.
      $(settings.sticky_preview.sticky_preview_selector, context).prepend(settings.sticky_preview.sticky_preview_box);

      // Add event bindings for all enabled in preview fields.
      for (var fieldName in settings.sticky_preview.sticky_preview_fields) {
        var $fieldInput = $('[name*="' + fieldName + '"]');

        if ($fieldInput.length) {
          var $item = $('#' + settings.sticky_preview.sticky_preview_fields[fieldName].field_item_id),
              $placeholder = $('#' + settings.sticky_preview.sticky_preview_fields[fieldName].field_placeholder_id);

          // Add error class for fields that has not passed validation.
          if ($fieldInput.hasClass('error')) {
            $item.addClass('error');
          }

          // Default values for preview box.
          $placeholder.html(Drupal.stickyPreview.stickyPreviewProcessInput(fieldName, context));

          // Update values in preview box.
          $fieldInput.bind(Drupal.stickyPreview.getEventForInput($fieldInput), function() {
            var thisName = $(this).attr('name').replace(/\[.*?\]/g, ''), // Get rid of [und][0][value] etc.
                $thisPlaceholder = $('#' + settings.sticky_preview.sticky_preview_fields[thisName].field_placeholder_id);

            $thisPlaceholder.html(Drupal.stickyPreview.stickyPreviewProcessInput(thisName));
          });
        }
      }
    }
  };

  /**
   * Behaviour provides show/hide functionality for sticky preview box.
   */
  Drupal.behaviors.stickyPreviewHide = {
    attach: function (context, settings) {
      $('.sticky-preview-wrapper .button', context).click(function() {
        $(this).closest('.sticky-preview-wrapper').toggleClass('opened closed');
      });
    }
  };
})(jQuery);
