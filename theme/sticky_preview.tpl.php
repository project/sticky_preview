<?php
/**
 * @file
 * sticky_preview.tpl.php file.
 *
 *  Available custom variables:
 *  - $sticky_preview_title.
 *  - $sticky_preview_fields.
 *  - $sticky_preview_default_state.
 *  - $sticky_preview_default_position.
 */
?>

<div class="sticky-preview-wrapper <?php print $sticky_preview_default_state; ?> <?php print $sticky_preview_default_position; ?>">
  <div class="control-bar">
    <div class="button minimize">
      <div class="icon"></div>
    </div>
  </div>
  <div class="content">
    <h2 class="sticky-preview-title"><?php print t($sticky_preview_title . ' preview'); ?></h2>
    <ul class="fields-listr">
      <?php foreach ($sticky_preview_fields as $key => $field_info): ?>
        <li id="<?php print $field_info['field_item_id']; ?>" class="sticky-preview-field-item">
          <h3 class="sticky-preview-field-title">
            <?php print t($field_info['label']); ?>
            <?php if ($field_info['required']): ?>
              <span class="form-required" title="<?php print t('This field is required.'); ?>">*</span>
            <?php endif; ?>
          </h3>
          <div id="<?php print $field_info['field_placeholder_id']; ?>" class="sticky-preview-field-placeholder"></div>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
